FROM golang:1.20-alpine

WORKDIR /app

COPY go.sum ./
COPY go.mod ./
COPY ./parser ./parser

RUN go mod download

RUN ls

RUN go build -o ./parser/cmd/main ./parser/cmd

WORKDIR /app/parser/cmd

CMD ["./main"]
