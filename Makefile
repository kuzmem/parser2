.PHONY: up

up:
	docker-compose -f nginx/docker-compose.yml up -d
	docker-compose -f selenium/docker-compose.yml up -d

	docker-compose -f service1/docker-compose.yml up -d
	docker-compose -f service2/docker-compose.yml up -d
	docker-compose -f swagger/docker-compose.yml up -d
