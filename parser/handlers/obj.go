package handlers

type ReqBodyQuery struct {
	Query string `json:"query"`
}
type ReqBodyID struct {
	ID int `json:"id"`
}

type ResBodyVacCount struct {
	Count int `json:"Vacancy count"`
}
