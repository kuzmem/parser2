package repo

import (
	"fmt"
	"gitlab.com/kuzmem/parser2/parser/domain/entity"
	"sync"
)

type VacancyRepo struct {
	data               []entity.Vacancy
	primaryKeyID       map[int]entity.Vacancy
	autoIncrementCount int
	sync.Mutex
}

func NewVacancyRepo() *VacancyRepo {
	return &VacancyRepo{
		data:               []entity.Vacancy{},
		primaryKeyID:       make(map[int]entity.Vacancy),
		autoIncrementCount: 1,
	}
}

func (v *VacancyRepo) Create(dto entity.Vacancy) {
	v.Lock()
	defer v.Unlock()
	v.primaryKeyID[v.autoIncrementCount] = dto
	v.data = append(v.data, dto)
	v.autoIncrementCount++
}

func (v *VacancyRepo) GetByID(id int) (entity.Vacancy, error) {
	out, ok := v.primaryKeyID[id]
	if !ok {
		return entity.Vacancy{}, fmt.Errorf("not found")
	}
	return out, nil
}

func (v *VacancyRepo) GetList() ([]entity.Vacancy, error) {
	if len(v.data) < 1 {
		return nil, fmt.Errorf("list is empty")
	}
	return v.data, nil
}

func (v *VacancyRepo) Delete(id int) error {
	v.Lock()
	defer v.Unlock()

	if _, ok := v.primaryKeyID[id]; !ok {
		return fmt.Errorf("not found")
	}
	v.data = append(v.data[:id-1], v.data[id:]...)
	newPrimaryKey := make(map[int]entity.Vacancy, len(v.data))
	for i, vac := range v.data {
		newPrimaryKey[i+1] = vac
	}
	v.primaryKeyID = newPrimaryKey
	v.autoIncrementCount--
	return nil
}
